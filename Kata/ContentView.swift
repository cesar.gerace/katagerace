//
//  ContentView.swift
//  Kata
//
//  Created by César Gerace on 06/10/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
        }.onAppear(){
            
                let gameBoard = GameBoard(slots: createSlots(), printer: SlotPrinter())
            gameBoard.printSlots()
        }
        .padding()
    }
    
    private func createSlots() -> [Slot] {
        var slots = [Slot]()
        for slotPosition in 1...62 {
            slots.append(Slot(position: slotPosition))
        }
        return slots
    }
    
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

