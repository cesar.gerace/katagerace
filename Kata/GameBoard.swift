//
//  GameBoard.swift
//  Kata
//
//  Created by César Gerace on 12/10/2022.
//

import Foundation


struct GameBoard {
    let slots: [Slot]
    let printer: SlotPrinter
    
    func printSlots() {
        slots.forEach{
            printer.printSlotMessageFor($0)
        }
    }
}
