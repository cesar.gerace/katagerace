//
//  SlotPrinter.swift
//  Kata
//
//  Created by César Gerace on 12/10/2022.
//

import Foundation

struct SlotPrinter {
    func printSlotMessageFor(_ slot: Slot) {
        print(slot.shouldMoveTo())
    }
}
