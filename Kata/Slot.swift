//
//  Slot.swift
//  Kata
//
//  Created by César Gerace on 12/10/2022.
//

import Foundation

struct Slot {
    let position: Int
    
    func shouldMoveTo() -> String {
        if position == 6 {
            return "The Bridge: Go to space 12"
        }else if position % 6 == 0 {
            return "Move two spaces forward."
        }else {
            return "Stay in space \(position)"
        }
    }
}
