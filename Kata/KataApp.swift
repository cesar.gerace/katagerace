//
//  KataApp.swift
//  Kata
//
//  Created by César Gerace on 06/10/2022.
//

import SwiftUI

@main
struct KataApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
