//
//  KataTests.swift
//  KataTests
//
//  Created by César Gerace on 06/10/2022.
//

import XCTest
@testable import Kata




final class SlotsTests: XCTestCase {
    
    func testSlotSaysStayInSpaceOneForSlot1() {
        let slot = Slot(position: 1)
        XCTAssertEqual(slot.shouldMoveTo(), "Stay in space 1")
    }
    
    func testSlotSaysStayInSpaceTwoForSlot2() {
        let slot = Slot(position: 2)
        XCTAssertEqual(slot.shouldMoveTo(), "Stay in space 2")
    }
    
    func testSlotSaysGoToSpace12ForSlot6() {
        let slot = Slot(position: 6)
        XCTAssertEqual(slot.shouldMoveTo(), "The Bridge: Go to space 12")
    }
    
    func testSlotSaysMoveTwoSpacesForSlot12() {
        let slot = Slot(position: 12)
        XCTAssertEqual(slot.shouldMoveTo(), "Move two spaces forward.")
    }
    
    func testSlotSaysMoveTwoSpacesForSlot18() {
        let slot = Slot(position: 18)
        XCTAssertEqual(slot.shouldMoveTo(), "Move two spaces forward.")
    }
}




final class GameBoardTests: XCTestCase {
    
    private func createSlots() -> [Slot] {
        var slots = [Slot]()
        for slotPosition in 1...62 {
            slots.append(Slot(position: slotPosition))
        }
        return slots
    }
  
    func testSlot1IsCreated() {
        let board = GameBoard(slots: createSlots(), printer: SlotPrinter())
        
        XCTAssertEqual(board.slots[0].position, 1)
    }
    
    func testSlot2IsCreated() {
        let board = GameBoard(slots: createSlots(), printer: SlotPrinter())
        XCTAssertEqual(board.slots[1].position, 2)
    }
    
    func testSlot62IsCreated() {
        let board = GameBoard(slots: createSlots(), printer: SlotPrinter())
        XCTAssertEqual(board.slots[61].position, 62)
    }
    
    func testPrinterIsCreated() {
        let board = GameBoard(slots: createSlots(), printer: SlotPrinter())
        
        XCTAssertNotNil(board.printer)
    }
}
